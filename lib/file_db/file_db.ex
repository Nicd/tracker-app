defmodule TrackerApp.FileDB do
  @moduledoc """
  Flat file key-value store where each key represents a filename and values are stored in those
  files using `:erlang.term_to_binary/2`.
  """

  @file_prefix "filedb_"
  @file_extension "dat"

  @typedoc "ID (key) for data, only use `[a-zA-Z0-9]` for safety."
  @type id :: String.t()

  @spec has_id?(id()) :: boolean()
  def has_id?(id) do
    id |> path_to() |> File.exists?()
  end

  @spec read(id()) :: {:ok, term()} | {:error, File.posix()}
  def read(id) do
    with {:ok, data} <- File.read(path_to(id)), do: {:ok, from_file_format(data)}
  end

  @spec write(id(), term()) :: :ok | {:error, File.posix()}
  def write(id, data) do
    File.write(path_to(id), to_file_format(data))
  end

  @spec delete(id()) :: :ok | {:error, File.posix()}
  def delete(id) do
    id |> path_to() |> File.rm()
  end

  @spec to_file_format(term()) :: binary()
  defp to_file_format(data) do
    :erlang.term_to_binary(data, minor_version: 2, compressed: db_compression())
  end

  @spec from_file_format(binary()) :: term()
  defp from_file_format(data) do
    :erlang.binary_to_term(data)
  end

  @spec path_to(id()) :: String.t()
  defp path_to(<<prefix::binary-size(2), _rest::binary>> = id) do
    Path.join([db_path(), prefix, "#{@file_prefix}#{id}.#{@file_extension}"])
  end

  @spec db_path() :: String.t()
  defp db_path(),
    do: Application.get_env(:tracker_app, :file_db_path) || raise("Missing FileDB path!")

  @spec db_compression() :: 0..9
  defp db_compression(), do: Application.get_env(:tracker_app, :file_db_compression, 0)
end
