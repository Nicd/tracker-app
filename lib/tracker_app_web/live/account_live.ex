defmodule TrackerAppWeb.AccountLive do
  use TrackerAppWeb, :live_view

  alias TrackerApp.ItemDB.Schemas.Account
  alias TrackerApp.AccountManager

  @impl true
  def mount(%{"accountId" => id}, _session, socket) do
    manager =
      case AccountManager.start(%AccountManager.Options{id: id}) do
        {:ok, pid} -> pid
        {:error, {:already_started, pid}} -> pid
        {:error, :account_not_found} -> :account_not_found
      end

    if manager != :account_not_found do
      account =
        if connected?(socket) do
          AccountManager.register(manager)
        else
          AccountManager.get_data(manager)
        end

      socket = common_assigns(socket, account)
      {:ok, assign(socket, account: account, manager: manager)}
    else
      {:ok, assign(socket, :account, :not_found)}
    end
  end

  @impl true
  def handle_event(event, params, socket)

  def handle_event("add-item", _params, socket) do
    {:noreply,
     push_redirect(socket,
       to:
         Routes.item_path(
           socket,
           socket.assigns.live_action,
           socket.assigns.account.id
         )
     )}
  end

  def handle_event("edit-item", params, socket) do
    item = Account.find_item_by_id(socket.assigns.account, params["id"])

    {:noreply,
     push_redirect(socket,
       to:
         Routes.item_path(socket, socket.assigns.live_action, socket.assigns.account.id, item.id)
     )}
  end

  @impl true
  def handle_info(msg, socket)

  def handle_info({AccountManager, :data_updated, %Account{} = account}, socket) do
    socket = common_assigns(socket, account)
    {:noreply, assign(socket, account: account)}
  end

  @spec common_assigns(Phoenix.LiveView.Socket.t(), Account.t()) :: Phoenix.LiveView.Socket.t()
  defp common_assigns(socket, %Account{} = account) do
    # TODO: UTC
    today = Date.utc_today()

    {expired, nonexpired} =
      Enum.split_with(account.items, &(Date.compare(&1.expiry_date, today) == :lt))

    assign(
      socket,
      expired: expired,
      nonexpired: nonexpired
    )
  end
end
