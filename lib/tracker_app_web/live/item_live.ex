defmodule TrackerAppWeb.ItemLive do
  use TrackerAppWeb, :live_view

  alias TrackerApp.ItemDB.Schemas.{Account, Item, Types}
  alias TrackerApp.AccountManager

  @impl true
  def mount(%{"accountId" => id} = params, _session, socket) do
    socket = assign(socket, invalid: false)

    manager =
      case AccountManager.start(%AccountManager.Options{id: id}) do
        {:ok, pid} -> pid
        {:error, {:already_started, pid}} -> pid
        {:error, :account_not_found} -> :account_not_found
      end

    if manager != :account_not_found do
      account =
        if connected?(socket) do
          AccountManager.register(manager)
        else
          AccountManager.get_data(manager)
        end

      item_id = Map.get(params, "itemId")

      {is_new?, item} =
        if is_nil(item_id) do
          {true, Item.new()}
        else
          {false, find_item_by_id(account.items, item_id) || :not_found}
        end

      {:ok, assign(socket, account: account, item: item, is_new?: is_new?, manager: manager)}
    else
      {:ok, assign(socket, account: :not_found)}
    end
  end

  @impl true
  def handle_event(event, params, socket)

  def handle_event("save-item", params, socket) do
    with true <- Map.has_key?(params, "name"),
         true <- Map.has_key?(params, "amount_unit"),
         {:ok, expiry_date} <- Date.from_iso8601(params["expiry"]),
         {amount, _} when amount >= 0 <- Integer.parse(params["amount"]),
         true <- String.length(params["name"]) > 0 do
      existing_item = find_item_by_id(socket.assigns.account.items, socket.assigns.item.id)

      item = %Item{
        socket.assigns.item
        | name: params["name"],
          amount: amount,
          expiry_date: expiry_date,
          amount_unit: params["amount_unit"]
      }

      items =
        if is_nil(existing_item) do
          [item | socket.assigns.account.items]
        else
          [item | Enum.filter(socket.assigns.account.items, &(&1.id != item.id))]
        end

      account = %Account{socket.assigns.account | items: items}
      :ok = AccountManager.update_data(socket.assigns.manager, account)

      {:noreply, push_redirect(socket, to: Routes.account_path(socket, :paid, account.id))}
    else
      _ ->
        {:noreply, assign(socket, invalid: true)}
    end
  end

  def handle_event("delete-item", _params, socket) do
    existing_item = find_item_by_id(socket.assigns.account.items, socket.assigns.item.id)

    items =
      if not is_nil(existing_item) do
        Enum.filter(socket.assigns.account.items, &(&1.id != socket.assigns.item.id))
      else
        # Item had already been deleted, do nothing
        socket.assigns.account.items
      end

    account = %Account{socket.assigns.account | items: items}
    :ok = AccountManager.update_data(socket.assigns.manager, account)

    {:noreply, push_redirect(socket, to: Routes.account_path(socket, :paid, account.id))}
  end

  @impl true
  def handle_info(msg, socket)

  def handle_info({AccountManager, :data_updated, %Account{} = account}, socket) do
    {:noreply, assign(socket, account: account)}
  end

  @spec find_item_by_id([Item.t()], Types.id()) :: Item.t() | nil
  defp find_item_by_id(items, id) do
    Enum.find(items, &(&1.id == id))
  end
end
