<%= if @account == :not_found do %>
  <%= render(TrackerAppWeb.UtilsView, "account_not_found.html", []) %>
<% else %>
  <div class="item-editor">
    <%= if @item == :not_found do %>
      <div class="not-found-error">
        <p>This item was not found.</p>

        <%= live_redirect(to: Routes.account_path(@socket, :paid, @account.id)) do %>
          Go back to main view.
        <% end %>
      </div>
    <% else %>
      <form phx-submit="save-item">
        <%= if @invalid do %>
          <div class="item-editor-error">
            There was an error with the input data, please check the values.
          </div>
        <% end %>

        <input name="id" type="hidden" value="<%= @item.id %>" />

        <input
          name="name"
          type="text"
          class="item-editor-name-input"
          value="<%= @item.name %>"
          required
        />

        <div class="item-editor-amount-group">
          <button
            type="button"
            class="item-editor-amount-dec"
            onclick="const e = document.getElementById('item-editor-amount'); e.value = Math.max(0, --e.value);"
          >−</button>
          <input
            id="item-editor-amount"
            name="amount"
            type="text"
            class="item-editor-amount-input"
            value="<%= @item.amount %>"
            inputmode="numeric"
            pattern="[0-9]*"
            required
          />
          <button
            type="button"
            class="item-editor-amount-inc"
            onclick="++document.getElementById('item-editor-amount').value;"
          >+</button>

          <input
            name="amount_unit"
            type="text"
            class="item-editor-amount-unit-input"
            value="<%= @item.amount_unit %>"
          />
        </div>

        <div class="item-editor-expiry">
          <% ed = if not is_nil(@item.expiry_date), do: Date.to_iso8601(@item.expiry_date) , else: "" %>
          <input
            name="expiry"
            type="date"
            min="<%= Date.utc_today() |> Date.to_iso8601() %>"
            value="<%= ed %>"
            required
          />
        </div>

        <%= if not @is_new? do %>
          <button
            type="button"
            class="item-editor-remove"
            phx-click="delete-item"
            phx-value-id="<%= @item.id %>"
          >🗑</button>
        <% end %>

        <button
          type="submit"
          class="item-editor-save"
        >✓</button>
      </form>
    <% end %>
  </div>
<% end %>
