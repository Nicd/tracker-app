defmodule TrackerAppWeb.RegisterLive do
  use TrackerAppWeb, :live_view

  alias TrackerApp.ItemDB

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_event(evt, params, socket)

  def handle_event("create", _, socket) do
    with account <- ItemDB.create_account(),
         :ok <- ItemDB.update_account(account) do
      {:noreply, push_redirect(socket, to: Routes.account_path(socket, :paid, account.id))}
    end
  end
end
