defmodule TrackerApp.Live.Components.ItemEditor do
  use Phoenix.LiveComponent

  @impl true
  def update(assigns, socket) do
    assigns = Map.put(assigns, :invalid, false)
    {:ok, %{socket | assigns: Map.merge(socket.assigns, assigns)}}
  end

  @impl true
  def handle_event("save-item", params, socket) do
    IO.inspect(socket |> Map.to_list())
    {:noreply, socket}
  end
end
