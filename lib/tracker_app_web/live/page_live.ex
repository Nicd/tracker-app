defmodule TrackerAppWeb.PageLive do
  use TrackerAppWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end
end
