defmodule TrackerAppWeb.Router do
  use TrackerAppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {TrackerAppWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TrackerAppWeb do
    pipe_through :browser

    live "/", PageLive, :index
    live "/register/free", RegisterLive, :free
    live "/register/paid", RegisterLive, :paid

    live "/free", AccountLive, :free
    live "/account/:accountId", AccountLive, :paid

    live "/free/item/new", ItemLive, :free
    live "/free/item/:itemId", ItemLive, :free
    live "/account/:accountId/item/new", ItemLive, :paid
    live "/account/:accountId/item/:itemId", ItemLive, :paid
  end

  # Other scopes may use custom stacks.
  # scope "/api", TrackerAppWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: TrackerAppWeb.Telemetry
    end
  end
end
