defmodule TrackerApp.AccountManager do
  @moduledoc """
  Account manager is responsible for loading account information from files and saving it back,
  plus updating listening processes about the changes in information.
  """

  use GenServer, restart: :transient

  require Logger

  import TrackerApp.Utils.TypedStruct

  alias TrackerApp.ItemDB
  alias TrackerApp.ItemDB.Schemas.{Account, Types}

  @alive_check_interval 10 * 1_000

  defmodule Options do
    deftypedstruct(%{
      id: Types.id()
    })
  end

  defmodule State do
    deftypedstruct(%{
      data: Account.t(),
      update_ref: {reference() | nil, nil}
    })
  end

  @spec start_link(Options.t()) :: GenServer.on_start()
  def start_link(%Options{} = options) do
    GenServer.start_link(__MODULE__, %{id: options.id},
      name: {:via, Registry, {__MODULE__.NameRegistry, id_to_name(options.id)}}
    )
  end

  ### SERVER API

  @impl true
  @spec init(%{id: Types.id()}) :: {:ok, State.t()} | {:stop, :account_not_found}
  def init(%{id: id}) do
    Logger.debug("Starting AccountManger for #{id}...")

    case ItemDB.get_account(id) do
      {:ok, account} ->
        schedule_check()
        {:ok, %State{data: account}}

      {:error, _} ->
        {:stop, :account_not_found}
    end
  end

  @impl true
  def handle_call(msg, from, state)

  def handle_call(:get_data, _from, %State{} = state) do
    {:reply, state.data, state}
  end

  def handle_call({:update_data, %Account{} = account}, _from, %State{} = state) do
    case ItemDB.update_account(account) do
      :ok ->
        Logger.debug("Account data updated for #{state.data.id}, updating listeners...")
        listeners = Registry.lookup(__MODULE__.ClientRegistry, id_to_name(state.data.id))
        Enum.each(listeners, &send(&1 |> elem(0), {__MODULE__, :data_updated, account}))
        {:reply, :ok, %State{state | data: account}}

      {:error, term} ->
        {:reply, {:error, term}, state}
    end
  end

  @impl true
  def handle_info(msg, state)

  def handle_info(:check_alive, %State{} = state) do
    with results when results != [] <-
           Registry.lookup(__MODULE__.ClientRegistry, id_to_name(state.data.id)),
         alive when alive != [] <- Enum.filter(results, &(&1 |> elem(0) |> Process.alive?())) do
      schedule_check()
      {:noreply, state}
    else
      _ ->
        Logger.debug("No more listeners, AccountManager for #{state.data.id} closing...")
        :ok = ItemDB.update_account(state.data)
        {:stop, :normal, state}
    end
  end

  ### CLIENT API

  @spec start(Options.t()) :: DynamicSupervisor.on_start_child()
  def start(%Options{} = options) do
    DynamicSupervisor.start_child(__MODULE__.Supervisor, {__MODULE__, options})
  end

  @spec get_data(GenServer.name()) :: Account.t()
  def get_data(server) do
    GenServer.call(server, :get_data)
  end

  @spec update_data(GenServer.name(), Account.t()) :: :ok
  def update_data(server, %Account{} = account) do
    GenServer.call(server, {:update_data, account})
  end

  @spec register(GenServer.name()) :: Account.t()
  def register(server) do
    account = get_data(server)
    {:ok, _} = Registry.register(__MODULE__.ClientRegistry, id_to_name(account.id), nil)
    account
  end

  @spec schedule_check() :: reference()
  defp schedule_check(), do: Process.send_after(self(), :check_alive, @alive_check_interval)

  @spec id_to_name(Types.id()) :: String.t()
  defp id_to_name(id), do: "account:#{id}"
end
