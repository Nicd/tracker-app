defmodule TrackerApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      TrackerAppWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: TrackerApp.PubSub},
      # Start the Endpoint (http/https)
      TrackerAppWeb.Endpoint,
      # Start a worker by calling: TrackerApp.Worker.start_link(arg)
      # {TrackerApp.Worker, arg}
      {Registry, keys: :unique, name: TrackerApp.AccountManager.NameRegistry},
      {Registry, keys: :duplicate, name: TrackerApp.AccountManager.ClientRegistry},
      {DynamicSupervisor, strategy: :one_for_one, name: TrackerApp.AccountManager.Supervisor}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrackerApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    TrackerAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
