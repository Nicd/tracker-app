defmodule TrackerApp.ItemDB do
  @moduledoc """
  Database for account and item data, uses FileDB as a backend.
  """

  alias TrackerApp.FileDB
  alias TrackerApp.ItemDB.Schemas.Account

  @doc """
  Check if account exists with given id.
  """
  @spec account_exists?(Account.id()) :: boolean()
  def account_exists?(id) do
    FileDB.has_id?(id)
  end

  @doc """
  Create new empty account. Does not store the account, just returns it.
  """
  @spec create_account() :: Account.t()
  def create_account() do
    Account.new()
  end

  @spec get_account(Account.id()) :: {:ok, Account.t()} | {:error, term()}
  def get_account(id) do
    case FileDB.read(id) do
      {:ok, data} -> {:ok, Account.from_storage(data)}
      {:error, posix} -> {:error, "Unable to read file due to: #{inspect(posix)}"}
    end
  end

  @spec update_account(Account.t()) :: :ok | {:error, term()}
  def update_account(account) do
    case FileDB.write(account.id, Account.to_storage(account)) do
      :ok -> :ok
      {:error, posix} -> {:error, "Unable to write file due to: #{inspect(posix)}"}
    end
  end

  @spec delete_account(Account.id()) :: :ok | {:error, String.t()}
  def delete_account(id) do
    case FileDB.delete(id) do
      :ok -> :ok
      {:error, posix} -> {:error, "Unable to delete file due to: #{inspect(posix)}"}
    end
  end
end
