defmodule TrackerApp.ItemDB.Schemas.Item do
  import TrackerApp.Utils.TypedStruct

  deftypedstruct(%{
    id: TrackerApp.ItemDB.Schemas.Types.id(),
    name: {String.t(), ""},
    expiry_date: {Date.t() | nil, nil},
    amount: {pos_integer(), 0},
    amount_unit: {String.t() | nil, nil}
  })

  @doc """
  Create new item structure.
  """
  @spec new() :: t()
  def new() do
    %__MODULE__{
      id: UUID.uuid4()
    }
  end

  @spec to_storage(t()) :: map()
  def to_storage(item), do: Map.from_struct(item)

  @spec from_storage(map(), TrackerApp.ItemDB.Schemas.Types.version()) :: t()
  def from_storage(data, version)

  def from_storage(data, 1) do
    %__MODULE__{
      id: data.id,
      name: data.name,
      expiry_date: data.expiry_date,
      amount: data.amount,
      amount_unit: data.amount_unit
    }
  end
end
