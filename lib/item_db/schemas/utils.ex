defmodule TrackerApp.ItemDB.Schemas.Utils do
  @id_re ~R/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/

  def id_re(), do: @id_re
end
