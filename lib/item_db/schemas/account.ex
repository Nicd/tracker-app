defmodule TrackerApp.ItemDB.Schemas.Account do
  import TrackerApp.Utils.TypedStruct

  alias TrackerApp.ItemDB.Schemas.{Item, Types}

  @current_version 1

  deftypedstruct(%{
    id: Types.id(),
    items: {[Item.t()], []},
    paid_until: {Date.t() | nil, nil},
    version: {1, @current_version}
  })

  @doc """
  Create new account structure.
  """
  @spec new() :: t()
  def new() do
    %__MODULE__{
      id: UUID.uuid4()
    }
  end

  @spec to_storage(t()) :: map()
  def to_storage(account) do
    items = Enum.map(account.items, &Item.to_storage/1)

    %{
      id: account.id,
      items: items,
      paid_until: account.paid_until,
      version: @current_version
    }
  end

  @spec from_storage(map()) :: Account.t()
  def from_storage(data), do: from_storage(data, data.version)

  @spec from_storage(map(), Types.version()) :: Account.t()
  def from_storage(data, version)

  def from_storage(data, 1) do
    items = Map.get(data, :items) |> Enum.map(&Item.from_storage(&1, 1))

    %__MODULE__{
      id: data.id,
      items: items,
      paid_until: data.paid_until,
      version: 1
    }
  end

  @spec find_item_by_id(t(), Types.id()) :: Item.t() | nil
  def find_item_by_id(account, id) do
    Enum.find(account.items, &(&1.id == id))
  end
end
