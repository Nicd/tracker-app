defmodule TrackerApp.ItemDB.Schemas.Types do
  @typedoc "Version of data in storage."
  @type version :: 1

  @typedoc "UUIDv4 used as object identifier"
  @type id :: String.t()
end
